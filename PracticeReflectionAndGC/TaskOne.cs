﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace PracticeReflectionAndGC
{
    public abstract class TaskOne
    {
        public static void ImplementationOfTaskOne()
        {
            Console.WriteLine("Задание №1");
            Type type = typeof(string);
            Type[] parameterTypes = { typeof(int) };
            MethodInfo method = type.GetMethod("Substring", parameterTypes);
            object[] arguments = { 9 };
            object returnValue = method.Invoke("Какая-то строка", arguments);
            Console.WriteLine($"Результат: {returnValue}");
        }
    }
}

