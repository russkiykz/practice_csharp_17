﻿using System;

namespace PracticeReflectionAndGC
{
    class Program
    {
        static void Main(string[] args)
        {
            TaskOne.ImplementationOfTaskOne();
            Console.WriteLine();
            TaskTwo.ImplementationOfTaskTwo();
        }
    }
}
