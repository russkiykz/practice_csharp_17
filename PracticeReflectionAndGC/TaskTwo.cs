﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PracticeReflectionAndGC
{
    public abstract class TaskTwo
    {
        public static void ImplementationOfTaskTwo()
        {
            Console.WriteLine("Задание №2");
            foreach (var constructorInfo in typeof(List<>).GetConstructors())
            {
                Console.WriteLine(constructorInfo);
            }
        }
    }
}
